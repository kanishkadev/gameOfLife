This is my implimentation of the Game of Life. A table is created with each <td> representing a single cell. Cells with too few or too many neighbors will die. Cells with the proper number of neighbors will continue living or come alive. You have a few tasks for this interview exercise. Feel free to add anything extra. I'll put a few suggestions. Avoid using any external libraries (especially jquery) unless you think it'll add a lot of value. Have fun with it, and please create a merge request when you've finished.

**These you must do**

1. There are a few bugs to fix. Find them, fix them, add comments.
2. There's a function "changeInterval" that needs to be written. Write the function to accept an inputed number of milliseconds to change the interval to. Input can be down through a prompt. **Bonus points for styling an input box.**
3. Randomize the color of the <td> when a new cell is generated.
4. Display a running number of the total amount of cells currently alive.
5. Change the text of the play button to pause while the game is active.

**These are suggestions for things to do**

-	Impliment this as a <canvas> element.
-	add a theme to the page to make it generally more visually appealing
-	add a set of buttons that contain preset board settings that create interesting patterns
- Add ways to enter dynamic values for the conditions for cells to die and reproduce
-	If the cell has settled into a stable pattern, make it noticeably flashing
- Redo the calculations so that the table line has wrapping. In other words, the last <td> of a <tr> can affect the calculations of the first <td> in the <tr>. the same goes for the <tr>'s above and below

Good luck and all the very best.
