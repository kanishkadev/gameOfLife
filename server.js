var path = require('path');
var http = require('http');
var server = http.createServer();

var express = require('express');
var app = express();
server.on('request', app);

var isProduction = process.env.NODE_ENV === 'production';
var port = isProduction ? process.env.PORT : 1337;

server.listen(port, function() {
  console.log('The server is listening on port ' + port);
});

app.use(express.static(path.join(__dirname, 'browser')));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'gol.html'));
});

module.exports = app;
