var inicount = 0;
var aliveCount = 0;
var deadCount = 0;
var ranAliveCount = 0;
var ranDeadCount = 0;

var gameUtils = {

  getStatus(cell) {
    return cell.getAttribute('data-status');
  },
  setStatus(cell, statusToSet) {
    cell.className = statusToSet;
    cell.setAttribute('data-status', statusToSet);

  },




  toggleStatus(cell) {




   //BUG 3 - conditions were wrongly written.
    //if we receive alive cell mean the first condition was true in the function so we set the status as dead,
    // else means the second condition was true in the function so we see the cell status to alive, 
    //since the cell should keep alive or need re produce.

    if (gameUtils.getStatus(cell) === 'alive') {
      gameUtils.setStatus(cell, 'dead');
      deadCount++


      
    } else {
      gameUtils.setStatus(cell, 'alive');
      aliveCount++



    }

   // each time when there's a change in the board we get the number of times alive cell occurs and 
   // we get the number of times dead cell occurs, then we subtract two value as shown in bellow to
   // get the current Alive cell count

   var current_alive = aliveCount- deadCount;
   document.getElementById("alive").innerHTML = 'Number of Alive Cells in the Board : ' + current_alive;

   



  },
  selectCell(x, y) {


    return document.getElementById(x + '-' + y);
  },
  getNeighbors(cell) {

    var neighbors = [];

    var pos = cell.id.split('-').map(function(s) {
      return parseInt(s);
    });

    var sc = gameUtils.selectCell;

    // Same row adjacent.
    neighbors.push(sc(pos[0] - 1, pos[1]));
    neighbors.push(sc(pos[0] + 1, pos[1]));

    // Row above.
    neighbors.push(sc(pos[0] - 1, pos[1] - 1));
    neighbors.push(sc(pos[0], pos[1] - 1));
    neighbors.push(sc(pos[0] + 1, pos[1] - 1));

    // Row below.
    neighbors.push(sc(pos[0] - 1, pos[1] + 1));
    neighbors.push(sc(pos[0], pos[1] + 1));
    neighbors.push(sc(pos[0] + 1, pos[1] + 1));

    return neighbors.filter(function(cell) {
      return cell !== null;
    });

  },
  countLiveNeighbors: function(cell) {

    var neighbors = gameUtils.getNeighbors(cell);

    var liveNeighbors = neighbors.filter(function(neighbor) {
      return gameUtils.getStatus(neighbor) === 'alive';
    });


   
   
    // BUG 1 - Was returning the wrong live neighbors array length.
    // the alive neighbors array length was returning with plus one, it is wrong so we need to remove it
    return liveNeighbors.length;

  },


   changeInterval: function() {
 
     //Promot User to provide time intervel
     var time = prompt("Please Provide Change Time Interval : ");
     return time;
  }



};


var dimensions = 30;
var gameOfLife = {

  width: dimensions,
  height: dimensions,
  stepInterval: null,

  createAndShowBoard: function() {

    var goltable = document.createElement("tbody");
    var tablehtml = '';
    for (var h = 0; h < this.height; h++) {
      tablehtml += "<tr id='row+" + h + "'>";
      for (var w = 0; w < this.width; w++) {
        tablehtml += "<td data-status='dead' id='" + w + "-" + h + "'></td>";
      }
      tablehtml += "</tr>";
    }
    goltable.innerHTML = tablehtml;
    var board = document.getElementById('board');
    board.appendChild(goltable);
    this.setupBoardEvents();
  },

  forEachCell: function(iteratorFunc) {
    var cells = document.getElementsByTagName('td');
    [].slice.call(cells).forEach(function(cellElement) {
      var coords = cellElement.id.split('-');
      iteratorFunc(cellElement, parseInt(coords[0]), parseInt(coords[1]));
    });
  },

  setupBoardEvents: function() {


     document.getElementById("alive").innerHTML = 'Number of Alive Cells in the Board : ' + aliveCount;

    var onCellClick = function(e) {
      gameUtils.toggleStatus(this);
    };

    this.forEachCell(function(cell) {
      cell.onclick = onCellClick;
    });
    document.getElementById('board-size').onclick = this.changeDimensions.bind(this)
    document.getElementById('play_btn').onclick = this.enableAutoPlay.bind(this);
    document.getElementById('step_btn').onclick = this.step.bind(this);
    document.getElementById('clear_btn').onclick = this.clearBoard.bind(this);
    document.getElementById('reset_btn').onclick = this.createRandomBoard.bind(this);
 

  },

 

  changeDimensions: function() {
    if (this.width === size) {
      return
    }
    this.width = size.value
    this.height = size.value
    var board = document.getElementById('board')
    board.removeChild(board.firstChild)
    this.createAndShowBoard()
  },


 




  clearBoard: function() {

    this.forEachCell(function(cell) {
      //passing value empty we dont pass either dead or alive.
      gameUtils.setStatus(cell, '');
      document.getElementById("alive").innerHTML = 'Number of Alive Cells in the Board : ' + inicount;
      aliveCount = 0;
      deadCount = 0;

    });

    this.stop();

  },

  createRandomBoard: function() {


    ranAliveCount = 0;

    this.forEachCell(function(cell) {

      if (Math.random() > .5) {
        gameUtils.setStatus(cell, 'alive');
        ranAliveCount++;

      } else {
        gameUtils.setStatus(cell, 'dead');
        ranDeadCount++;
      }

      
    });


       document.getElementById("alive").innerHTML = 'Number of Alive Cells in the Board : ' + ranAliveCount;
    
        console.log(ranAliveCount);

  },

  step: function() {
    var extinct = true
    var toToggle = [];

    
  

    
    this.forEachCell(function(cell) {

      var liveNeighborsCount = gameUtils.countLiveNeighbors(cell);

    

      if (gameUtils.getStatus(cell) === 'alive') {

       // BUG 2 - Wrong algorithm, earlier code was not following the Conway's Life Of Game Rules.
       // check the alive cell Neighbor count is < 2 or > 3, if the condition is true then push the cell as 'dead'

        if (liveNeighborsCount < 2 || liveNeighborsCount > 3) {
          toToggle.push(cell);
        }

      } 


      else {



        //any dead cell with 3 Neighbor's push as 'alive'
        if (liveNeighborsCount === 3) {
          toToggle.push(cell);
          extinct = false
           
        }
      }





    });

    toToggle.forEach(gameUtils.toggleStatus);

    if (extinct) this.stop()
  },



  enableAutoPlay: function() {
    var self = this;

    

   //Set the Play button text to 'Pause' when user clicks this button  
   document.getElementById('play_btn').value = 'Pause';


    if (this.stepInterval !== null) {
      this.stop();
    } else {

      //Get the time interval from the changeInterval function into interval_time variable
      var interval_time = gameUtils.changeInterval();

      this.stepInterval = setInterval(function() {
        self.step();
      }, interval_time);
    }

  },

  stop: function() {

    //Set Play button text back to 'Play' after simulation stops
    document.getElementById('play_btn').value = 'Play';
    clearInterval(this.stepInterval);
    this.stepInterval = null;

  }
};

gameOfLife.createAndShowBoard();
